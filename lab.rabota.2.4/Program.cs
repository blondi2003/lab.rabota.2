﻿using System;

namespace lab.rabota._2._4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Название компании
            string name = "Лукойл";
            //Специализация компании
            string name1 = "Нефтегазовая компания";
            //Дата основания
            DateTime date = new DateTime(1991, 11, 25);
            //Глава компании
            string name2 = "Алекперов Вагит Юсуфович";
            //Города,в которых были созданы первые организации
            string name3 = "Лангепас, Урай и Когалым";
            //Количество сотрудников
            int a = 200000;
            //Количество акций (млн)
            int b = 653;
            //Средняя стоимость акций
            int d = 6528;
            //Количество дочерних организаций
            int c = 49;
            //Активы фирмы
            int h = 6508931;
            //Оборотные активы
            int m = 1838580;
            //Краткосрочные обязательства 
            int n = 1278284;
            //Долги предприятия
            int i = 2203193;
            //Выручка компании
            int z = 4078367;
            //Чистая прибыль фирмы
            int q = 348363;
            //Собственный капитал
            int w = 4305738;
            //Текущая ликвидность
            double x = m / n;
            //Финансовая независимость
            double v = h / i;
            //Собственные оборотные средства
            int y = m - n;
            //Рентабельность собственного капитала
            int r = q / w;
            //Оборачиваемость собственного капитала
            int l = z / w;
            Console.WriteLine($"Название компании:{name}");
            Console.WriteLine($"Специализация компании:{name1}");
            Console.WriteLine($"Дата основания:{date}");
            Console.WriteLine($"Глава компании:{name2}");
            Console.WriteLine($"Первые города:{name3}");
            Console.WriteLine($"Количество сотрудников:{a}");
            Console.WriteLine($"Количество акций, млн:{b}");
            Console.WriteLine($"Средняя стоимость акций:{d}");
            Console.WriteLine($"Количество дочерних организаций:{c}");
            Console.WriteLine($"Активы фирмы,млн:{h}");
            Console.WriteLine($"Долги предприятия,млн:{i}");
            Console.WriteLine($"Оборотные активы,млн:{m}");
            Console.WriteLine($"Краткосрочные обязательства,млн:{n}");
            Console.WriteLine($"Выручка компании, млн:{z}");
            Console.WriteLine($"Чистая прибыль, млн:{q}");
            Console.WriteLine($"Собственный капитал, млн:{w}");
            Console.WriteLine($"Рентабельность собственного капитала:{r}");
            Console.WriteLine($"Оборачиваемость собственного капитала:{l}");                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
            Console.WriteLine($"Текущая ликвидность:{x}");
            Console.WriteLine($"Финансовая независимость:{v}");
            Console.WriteLine($"Собственные оборотные средства:{y}");
            Console.ReadKey();
        }
    }
}
