﻿using System;

namespace lab.rabota._2._3_
{
    class Program
    {
        static void Main(string[] args)
        {
            double f, x, y, z;
            x = double.Parse(Console.ReadLine());
            y = double.Parse(Console.ReadLine());
            z = double.Parse(Console.ReadLine());
            f = 1.0 / 19.0 + Math.Log(Math.Abs(y)) / 7.0 + z / 3 + Math.Max(x / 4, 2);
            Console.WriteLine(f);
            Console.ReadKey();

        }
    }
}
